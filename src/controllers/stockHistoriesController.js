// Dependencies
const StockHistories = require("../models/stockHistories");
const redisClient = require("../database/cacheDatabaseConnection").client;

module.exports.getStocks = async (req, res) => {
  // Query options
  const findOptions = {};

  // Query paramaters
  let { limit, year, name } = req.query;

  // Check if the year query params was defined
  if (year) {
    year = parseInt(year) + 1;
    year = year.toString();
    findOptions.date = { $lte: new Date(year).toISOString() };
  }

  // Filter by name
  if (name) {
    findOptions.name = name;
  }

  try {
    // Query stocks in the database
    let stocks = await StockHistories.find(findOptions).limit(
      parseInt(limit) || 50
    );

    // Cache the results in memory for 30 minutes until it expires
    if (stocks.length > 0) {
      redisClient.setex("stockHistories", 1800, JSON.stringify(stocks));
    }
    // Send the request response
    res.status(200).json(stocks);
  } catch (err) {
    // Send an error 400 response
    res.status(500).json(err);
  }
};

module.exports.getStockNames = async (req, res) => {
  try {
    const names = await StockHistories.collection.distinct('name');
    // Send the request response
    res.status(200).json(names);
  } catch (err) {
    // Send an error 400 response
    res.status(500).json(err);
  }

};
