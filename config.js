module.exports = {
  test: {
    SERVER_NAME: "localhost",
    DB_NAME: "stockHistoryService_test",
    DATASET: `${__dirname}/test/dataset`
  },
  dev: {
    SERVER_NAME: "localhost",
    DB_NAME: "stockHistoryService",
    DATASET: `${__dirname}/test/dataset`
  },
  production: {
    SERVER_NAME: "localhost",
    DB_NAME: "stockHistoryService_prod",
    DATASET: "../test/dataset"
  }
}
